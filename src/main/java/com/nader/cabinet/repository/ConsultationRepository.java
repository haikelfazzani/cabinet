package com.nader.cabinet.repository;

import com.nader.cabinet.model.Consultation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConsultationRepository extends CrudRepository<Consultation,Long> {
}
