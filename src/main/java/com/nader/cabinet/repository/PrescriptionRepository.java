package com.nader.cabinet.repository;

import com.nader.cabinet.model.Prescription;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PrescriptionRepository  extends CrudRepository<Prescription,Long> {
}
