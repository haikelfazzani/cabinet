package com.nader.cabinet.controller;

import com.nader.cabinet.model.Client;
import com.nader.cabinet.model.Rv;
import com.nader.cabinet.repository.ClientRepository;
import com.nader.cabinet.repository.MedecinRepository;
import com.nader.cabinet.repository.RvRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
public class RdvController {
    @Autowired
    RvRepository agent;
    @Autowired
    ClientRepository agentclient;
    @Autowired
    MedecinRepository agentmed;
    @Autowired
    private Environment env;

    @RequestMapping(value="/rdv/add",method = RequestMethod.GET)
    public ModelAndView add_rdv(HttpSession session){
        String email = (String) session.getAttribute("auth_session_email");
        String password = (String) session.getAttribute("auth_session_password");

        ModelAndView model = new ModelAndView();

        if(email != null && password != null && email.equals(env.getProperty("admin.email"))
                && password.equals(env.getProperty("admin.password"))) {
            List<Client> clients=(List<Client>)agentclient.findAll();
            Rv rdv= new Rv();
            model.addObject("clients",clients);
            model.addObject("rdvForm",rdv);
            model.setViewName("rendez_vous_add");
            return model;
        }
        else {
            return (new ModelAndView("redirect:/login"));
        }
    }

    @RequestMapping(value="/rdv/save",method = RequestMethod.POST)
    public ModelAndView save_rdv(@ModelAttribute("rdvForm") Rv rdv,HttpSession session){
        String email = (String) session.getAttribute("auth_session_email");
        String password = (String) session.getAttribute("auth_session_password");

        if(email != null && password != null && email.equals(env.getProperty("admin.email")) && password.equals(env.getProperty("admin.password"))) {
            agent.save(rdv);
            return new ModelAndView("redirect:/rdv/list");
        }
        else {
            return (new ModelAndView("redirect:/login"));
        }
    }

    @RequestMapping(value="/rdv/list",method = RequestMethod.GET)
    public ModelAndView list_rdv(HttpSession session){
        String email = (String) session.getAttribute("auth_session_email");
        String password = (String) session.getAttribute("auth_session_password");

        ModelAndView model = new ModelAndView();

        if(email != null && password != null && email.equals(env.getProperty("admin.email"))
                && password.equals(env.getProperty("admin.password"))) {
            List<Rv> rdvs=(List<Rv>) agent.findAll();
            model.addObject("rdvs",rdvs);
            model.setViewName("rendez_vous_list");
            return model;
        }
        else {
            return (new ModelAndView("redirect:/login"));
        }
    }

    @RequestMapping(value="/rdv/delete/{idm}",method = RequestMethod.GET)
    public ModelAndView delete_rdv(@PathVariable("idm") long idrdv,HttpSession session){
        String email = (String) session.getAttribute("auth_session_email");
        String password = (String) session.getAttribute("auth_session_password");
        try {
            if(email != null && password != null && email.equals(env.getProperty("admin.email"))
                    && password.equals(env.getProperty("admin.password"))) {
                agent.deleteById(idrdv);
                return (new ModelAndView("redirect:/rdv/list"));
            }
            else {
                return (new ModelAndView("redirect:/login"));
            }
        }
        catch (Exception e) {
            return (new ModelAndView("redirect:/rdv/list"));
        }
    }

    @RequestMapping(value = "/rdv/update/{idr}",method = RequestMethod.GET)
    public ModelAndView edit_rdv(@PathVariable("idr") long idrdv,HttpSession session){
        String email = (String) session.getAttribute("auth_session_email");
        String password = (String) session.getAttribute("auth_session_password");

        ModelAndView model = new ModelAndView();

        if(email != null && password != null && email.equals(env.getProperty("admin.email"))
                && password.equals(env.getProperty("admin.password"))) {
            Rv rd =  agent.findById(idrdv).get();
            List<Client> clients=(List<Client>)agentclient.findAll();
            model.addObject("rdvForm",rd);
            model.addObject("clients",clients);
            model.setViewName("rendez_vous_edit");
            return model;
        }
        else {
            return (new ModelAndView("redirect:/login"));
        }
    }
}
