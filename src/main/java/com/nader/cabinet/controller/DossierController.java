package com.nader.cabinet.controller;

import com.nader.cabinet.model.Client;
import com.nader.cabinet.model.Dossier;
import com.nader.cabinet.repository.ClientRepository;
import com.nader.cabinet.repository.DossierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
public class DossierController {
    @Autowired
    DossierRepository dossierRepository;
    @Autowired
    ClientRepository agentclient;
    @Autowired
    private Environment env;

    @RequestMapping(value="/dossier/list",method = RequestMethod.GET)
    public ModelAndView list(HttpSession session){
        String email = (String) session.getAttribute("auth_session_email");
        String password = (String) session.getAttribute("auth_session_password");

        ModelAndView model = new ModelAndView();

        if(email != null && password != null && email.equals(env.getProperty("admin.email"))
                && password.equals(env.getProperty("admin.password"))) {
            model.addObject("clients",agentclient.findAll());
            model.addObject("dossiers",dossierRepository.findAll());
            model.setViewName("dossier_list");
            return model;
        }
        else {
            return (new ModelAndView("redirect:/login"));
        }
    }

    @RequestMapping(value="/dossier/add",method = RequestMethod.GET)
    public ModelAndView add(HttpSession session){
        String email = (String) session.getAttribute("auth_session_email");
        String password = (String) session.getAttribute("auth_session_password");

        ModelAndView model = new ModelAndView();

        if(email != null && password != null && email.equals(env.getProperty("admin.email"))
                && password.equals(env.getProperty("admin.password"))) {
            List<Client> clients=(List<Client>)agentclient.findAll();
            Dossier dossier = new Dossier();
            model.addObject("dossierForm",dossier);
            model.addObject("clients",clients);
            model.setViewName("dossier_add");
            return model;
        }
        else {
            return (new ModelAndView("redirect:/login"));
        }
    }

    @RequestMapping(value="/dossier/save",method = RequestMethod.POST)
    public ModelAndView save(@ModelAttribute("dossierForm") Dossier dossier,HttpSession session){
        String email = (String) session.getAttribute("auth_session_email");
        String password = (String) session.getAttribute("auth_session_password");

        if(email != null && password != null && email.equals(env.getProperty("admin.email")) && password.equals(env.getProperty("admin.password"))) {
            dossierRepository.save(dossier);
            return new ModelAndView("redirect:/dossier/list");
        }
        else {
            return (new ModelAndView("redirect:/login"));
        }
    }
    @RequestMapping(value="/dossier/delete/{idm}",method = RequestMethod.GET)
    public ModelAndView delete(@PathVariable("idm") long idDossier,HttpSession session){
        String email = (String) session.getAttribute("auth_session_email");
        String password = (String) session.getAttribute("auth_session_password");
        try {
            if(email != null && password != null && email.equals(env.getProperty("admin.email"))
                    && password.equals(env.getProperty("admin.password"))) {
                dossierRepository.deleteById(idDossier);
                return (new ModelAndView("redirect:/dossier/list"));
            }
            else {
                return (new ModelAndView("redirect:/login"));
            }
        }
        catch (Exception e) {
            return (new ModelAndView("redirect:/dossier/list"));
        }
    }
    @RequestMapping(value = "/dossier/update/{idr}",method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable("idr") long idDossier,HttpSession session){
        String email = (String) session.getAttribute("auth_session_email");
        String password = (String) session.getAttribute("auth_session_password");

        ModelAndView model = new ModelAndView();

        if(email != null && password != null && email.equals(env.getProperty("admin.email"))
                && password.equals(env.getProperty("admin.password"))) {
            Dossier dossier =  dossierRepository.findById(idDossier).get();
            model.addObject("dossierForm",dossier);
            List<Client> clients=(List<Client>)agentclient.findAll();
            model.addObject("clients",clients);
            model.setViewName("dossier_edit");
            return model;
        }
        else {
            return (new ModelAndView("redirect:/login"));
        }
    }
}
