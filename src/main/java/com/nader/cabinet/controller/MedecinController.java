package com.nader.cabinet.controller;

import com.nader.cabinet.model.Medecin;
import com.nader.cabinet.repository.MedecinRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;

@RestController
public class MedecinController {
    @Autowired
    MedecinRepository agent;
    @RequestMapping(value = "/medecins/add",method = RequestMethod.GET)
    public ModelAndView add_med(){
        ModelAndView model =new ModelAndView();
        Medecin med = new Medecin();
        model.addObject("medForm",med);
        model.setViewName("Medecin_Form");
        return model;
    }
    @RequestMapping(value="/medecins/save",method = RequestMethod.POST)
    public ModelAndView save(@Valid @ModelAttribute("medForm") Medecin med, BindingResult result){
        if(result.hasErrors()){
            return (new ModelAndView("Medecin_Form"));
        }
        else {
            agent.save(med);
            return new ModelAndView("redirect:/medecins/list");
        }
    }
    // controlleur pour affichage de la list de médecins a partir de BD
    @RequestMapping(value="/medecins/list",method = RequestMethod.GET)
    public ModelAndView list_med(){
        List<Medecin> meds=(List<Medecin>) agent.findAll();
        ModelAndView model = new ModelAndView();
        model.addObject("meds",meds);
        model.setViewName("mes_medecins");
        return  model;
    }
}
