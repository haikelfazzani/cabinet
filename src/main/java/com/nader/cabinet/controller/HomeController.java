package com.nader.cabinet.controller;

import com.nader.cabinet.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@RestController
public class HomeController {

    @Autowired
    ClientRepository clientRepository;
    @Autowired
    private Environment env;

    @RequestMapping(value = "/",method = RequestMethod.GET)
    public ModelAndView home(HttpSession session) {
        String email = (String) session.getAttribute("auth_session_email");
        String password = (String) session.getAttribute("auth_session_password");

        ModelAndView model = new ModelAndView();

        if(email != null && password != null && email.equals(env.getProperty("admin.email"))
                && password.equals(env.getProperty("admin.password"))) {
            model.setViewName("index");
            return model;
        }
        else {
            return (new ModelAndView("redirect:/login"));
        }
    }
}
