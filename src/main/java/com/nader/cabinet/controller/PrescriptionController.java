package com.nader.cabinet.controller;

import com.nader.cabinet.model.Consultation;
import com.nader.cabinet.model.Prescription;
import com.nader.cabinet.repository.ConsultationRepository;
import com.nader.cabinet.repository.PrescriptionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
public class PrescriptionController {
    @Autowired
    ConsultationRepository consultationRepository;
    @Autowired
    private PrescriptionRepository prescriptionRepository;
    @Autowired
    private Environment env;

    @RequestMapping(value="/prescription/list",method = RequestMethod.GET)
    public ModelAndView list(HttpSession session){
        String email = (String) session.getAttribute("auth_session_email");
        String password = (String) session.getAttribute("auth_session_password");

        ModelAndView model = new ModelAndView();

        if(email != null && password != null && email.equals(env.getProperty("admin.email"))
                && password.equals(env.getProperty("admin.password"))) {
            model.addObject("prescriptions",prescriptionRepository.findAll());
            model.setViewName("prescription_list");
            return model;
        }
        else {
            return (new ModelAndView("redirect:/login"));
        }
    }

    @RequestMapping(value="/prescription/add",method = RequestMethod.GET)
    public ModelAndView add(HttpSession session){
        String email = (String) session.getAttribute("auth_session_email");
        String password = (String) session.getAttribute("auth_session_password");

        ModelAndView model = new ModelAndView();

        if(email != null && password != null && email.equals(env.getProperty("admin.email"))
                && password.equals(env.getProperty("admin.password"))) {
            List<Consultation> consultations=(List<Consultation>)consultationRepository.findAll();
            Prescription prescription = new Prescription();
            model.addObject("prescriptionForm",prescription);
            model.addObject("consultations",consultations);
            model.setViewName("prescription_add");
            return model;
        }
        else {
            return (new ModelAndView("redirect:/login"));
        }
    }

    @RequestMapping(value="/prescription/save",method = RequestMethod.POST)
    public ModelAndView save(@ModelAttribute("prescriptionForm") Prescription prescription,HttpSession session){
        String email = (String) session.getAttribute("auth_session_email");
        String password = (String) session.getAttribute("auth_session_password");

        if(email != null && password != null && email.equals(env.getProperty("admin.email")) && password.equals(env.getProperty("admin.password"))) {
            prescriptionRepository.save(prescription);
            return new ModelAndView("redirect:/prescription/list");
        }
        else {
            return (new ModelAndView("redirect:/login"));
        }
    }

    @RequestMapping(value="/prescription/delete/{idm}",method = RequestMethod.GET)
    public ModelAndView delete(@PathVariable("idm") long idPrescription,HttpSession session){
        String email = (String) session.getAttribute("auth_session_email");
        String password = (String) session.getAttribute("auth_session_password");
        try {
            if(email != null && password != null && email.equals(env.getProperty("admin.email"))
                    && password.equals(env.getProperty("admin.password"))) {
                prescriptionRepository.deleteById(idPrescription);
                return (new ModelAndView("redirect:/prescription/list"));
            }
            else {
                return (new ModelAndView("redirect:/login"));
            }
        }
        catch (Exception e) {
            return (new ModelAndView("redirect:/prescription/list"));
        }
    }

    @RequestMapping(value = "/prescription/update/{idr}",method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable("idr") long idConsulation,HttpSession session){
        String email = (String) session.getAttribute("auth_session_email");
        String password = (String) session.getAttribute("auth_session_password");

        ModelAndView model = new ModelAndView();

        if(email != null && password != null && email.equals(env.getProperty("admin.email"))
                && password.equals(env.getProperty("admin.password"))) {
            Prescription prescription =  prescriptionRepository.findById(idConsulation).get();
            List<Consultation> consultations=(List<Consultation>)consultationRepository.findAll();
            model.addObject("prescriptionForm",prescription);
            model.addObject("consultations",consultations);
            model.setViewName("prescription_edit");
            return model;
        }
        else {
            return (new ModelAndView("redirect:/login"));
        }
    }
}
