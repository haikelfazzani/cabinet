package com.nader.cabinet.controller;

import com.nader.cabinet.model.Consultation;
import com.nader.cabinet.model.Dossier;
import com.nader.cabinet.repository.ConsultationRepository;
import com.nader.cabinet.repository.DossierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping("/consultation")
public class ConsultationController {
    @Autowired
    ConsultationRepository consultationRepository;
    @Autowired
    DossierRepository dossierRepository;
    @Autowired
    private Environment env;

    @RequestMapping(value="/list",method = RequestMethod.GET)
    public ModelAndView list(HttpSession session){
        String email = (String) session.getAttribute("auth_session_email");
        String password = (String) session.getAttribute("auth_session_password");

        ModelAndView model = new ModelAndView();

        if(email != null && password != null && email.equals(env.getProperty("admin.email"))
                && password.equals(env.getProperty("admin.password"))) {
            model.addObject("dossiers",dossierRepository.findAll());
            model.addObject("consultations",consultationRepository.findAll());
            model.setViewName("consultation_list");
            return model;
        }
        else {
            return (new ModelAndView("redirect:/login"));
        }
    }

    @RequestMapping(value="/add",method = RequestMethod.GET)
    public ModelAndView add(HttpSession session){
        String email = (String) session.getAttribute("auth_session_email");
        String password = (String) session.getAttribute("auth_session_password");

        ModelAndView model = new ModelAndView();

        if(email != null && password != null && email.equals(env.getProperty("admin.email"))
                && password.equals(env.getProperty("admin.password"))) {
            model.addObject("consultationForm",new Consultation());
            List<Dossier> dossiers=(List<Dossier>)dossierRepository.findAll();
            model.addObject("dossiers",dossiers);
            model.setViewName("consultation_add");
            return model;
        }
        else {
            return (new ModelAndView("redirect:/login"));
        }
    }

    @RequestMapping(value="/save",method = RequestMethod.POST)
    public ModelAndView save(@ModelAttribute("consultationForm") Consultation consultation,HttpSession session){
        String email = (String) session.getAttribute("auth_session_email");
        String password = (String) session.getAttribute("auth_session_password");

        if(email != null && password != null && email.equals(env.getProperty("admin.email")) && password.equals(env.getProperty("admin.password"))) {
            consultationRepository.save(consultation);
            return new ModelAndView("redirect:/consultation/list");
        }
        else {
            return (new ModelAndView("redirect:/login"));
        }
    }

    @RequestMapping(value="/delete/{idm}",method = RequestMethod.GET)
    public ModelAndView delete(@PathVariable("idm") long idConsulation,HttpSession session){
        String email = (String) session.getAttribute("auth_session_email");
        String password = (String) session.getAttribute("auth_session_password");
        try {
            if(email != null && password != null && email.equals(env.getProperty("admin.email"))
                    && password.equals(env.getProperty("admin.password"))) {
                consultationRepository.deleteById(idConsulation);
                return (new ModelAndView("redirect:/consultation/list"));
            }
            else {
                return (new ModelAndView("redirect:/login"));
            }
        }
        catch (Exception e) {
            return (new ModelAndView("redirect:/consultation/list"));
        }
    }

    @RequestMapping(value = "/update/{idr}",method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable("idr") long idConsulation,HttpSession session){
        String email = (String) session.getAttribute("auth_session_email");
        String password = (String) session.getAttribute("auth_session_password");

        ModelAndView model = new ModelAndView();

        if(email != null && password != null && email.equals(env.getProperty("admin.email"))
                && password.equals(env.getProperty("admin.password"))) {
            Consultation consultation =  consultationRepository.findById(idConsulation).get();
            model.addObject("consultationForm",consultation);
            List<Dossier> dossiers=(List<Dossier>)dossierRepository.findAll();
            model.addObject("dossiers",dossiers);
            model.setViewName("consultation_edit");
            return model;
        }
        else {
            return (new ModelAndView("redirect:/login"));
        }
    }
}
