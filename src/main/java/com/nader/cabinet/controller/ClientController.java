package com.nader.cabinet.controller;

import com.nader.cabinet.model.Client;
import com.nader.cabinet.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/clients")
public class ClientController {
    @Autowired
    ClientRepository clientRepository;
    @Autowired
    private Environment env;

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public ModelAndView ajouter_client(HttpSession session) {
        String email = (String) session.getAttribute("auth_session_email");
        String password = (String) session.getAttribute("auth_session_password");

        ModelAndView model = new ModelAndView();

        if(email != null && password != null && email.equals(env.getProperty("admin.email"))
                && password.equals(env.getProperty("admin.password"))) {
            Client c = new Client();
            model.addObject("clientForm", c);
            model.setViewName("client_add");
            return model;
        }
        else {
            return (new ModelAndView("redirect:/login"));
        }
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ModelAndView add_client(@Valid @ModelAttribute("clientForm") Client cl, BindingResult result,HttpSession session) {
        String email = (String) session.getAttribute("auth_session_email");
        String password = (String) session.getAttribute("auth_session_password");

        if(email != null && password != null && email.equals(env.getProperty("admin.email")) && password.equals(env.getProperty("admin.password"))) {
            clientRepository.save(cl);
            return (new ModelAndView("redirect:/clients/list"));
        }
        else {
            return (new ModelAndView("redirect:/login"));
        }
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list_clients(HttpSession session) {
        String email = (String) session.getAttribute("auth_session_email");
        String password = (String) session.getAttribute("auth_session_password");

        ModelAndView model = new ModelAndView();

        if(email != null && password != null && email.equals(env.getProperty("admin.email"))
                && password.equals(env.getProperty("admin.password"))) {
            List<Client> clients = (List<Client>) clientRepository.findAll();
            model.addObject("clients", clients);
            model.setViewName("client_list");
            return model;
        }
        else {
            return (new ModelAndView("redirect:/login"));
        }
    }

    @RequestMapping(value = "/delete/{idc}", method = RequestMethod.GET)
    public ModelAndView delete_client(@PathVariable("idc") long idclient,HttpSession session) {
        String email = (String) session.getAttribute("auth_session_email");
        String password = (String) session.getAttribute("auth_session_password");
        try {
            if(email != null && password != null && email.equals(env.getProperty("admin.email"))
                    && password.equals(env.getProperty("admin.password"))) {
                clientRepository.deleteById(idclient);
                return new ModelAndView("redirect:/clients/list");
            }
            else {
                return (new ModelAndView("redirect:/login"));
            }
        }
        catch(Exception e) {
            return new ModelAndView("client_list");
        }
    }

    @RequestMapping(value = "/update/{idc}", method = RequestMethod.GET)
    public ModelAndView edit_client(@PathVariable("idc") long idclient,HttpSession session) {
        String email = (String) session.getAttribute("auth_session_email");
        String password = (String) session.getAttribute("auth_session_password");
        try {
            if(email != null && password != null && email.equals(env.getProperty("admin.email"))
                    && password.equals(env.getProperty("admin.password"))) {
                Optional<Client> cl = clientRepository.findById(idclient);
                ModelAndView model = new ModelAndView();
                model.addObject("clientForm", cl);
                model.setViewName("client_edit");
                return model;
            }
            else {
                return (new ModelAndView("redirect:/login"));
            }
        }
        catch (Exception e) {
            ModelAndView model = new ModelAndView();
            model.setViewName("client_list");
            return (model);
        }
    }
}
