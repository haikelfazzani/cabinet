package com.nader.cabinet.controller;

import com.nader.cabinet.model.Auth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
public class AuthController {
    @Autowired
    private Environment env;

    @RequestMapping(value = "/login",method = RequestMethod.GET)
    public ModelAndView test(HttpSession session) {
        String email = (String) session.getAttribute("auth_session_email");
        String password = (String) session.getAttribute("auth_session_password");

        if(email != null && password != null && email.equals(env.getProperty("admin.email"))
                && password.equals(env.getProperty("admin.password"))) {
            return new ModelAndView("redirect:/");
        }
        else {
            ModelAndView model = new ModelAndView();
            Auth auth = new Auth();
            model.addObject("loginForm", auth);
            model.setViewName("login");
            return model;
        }
    }

    @RequestMapping(value = "/login/check",method = RequestMethod.POST)
    public ModelAndView check(@ModelAttribute("loginForm") Auth auth, HttpServletRequest request) {
        request.getSession().setAttribute("auth_session_email", auth.getEmail());
        request.getSession().setAttribute("auth_session_password", auth.getPassword());

        ModelAndView model = new ModelAndView();

        if(auth.getEmail().equals(env.getProperty("admin.email"))
                && auth.getPassword().equals(env.getProperty("admin.password"))) {
            model.setViewName("login");
            return (new ModelAndView("redirect:/"));
        }
        else {
            model.addObject("loginError","Les données ne sont pas valides!");
            model.setViewName("login");
            return model;
        }
    }

    @RequestMapping(value = "/logout",method = RequestMethod.GET)
    public ModelAndView logout(HttpServletRequest request) {
        request.getSession().removeAttribute("auth_session_email");
        request.getSession().removeAttribute("auth_session_password");
        return (new ModelAndView("redirect:/"));
    }
}
