package com.nader.cabinet.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "clients")
public class Client implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotEmpty
    @Size(min=5,max=100)
    @Pattern(regexp = "[a-zA-Z]+",message = "must not contain special characters")
    private String nom;

    @NotEmpty
    @Size(min=5,max=100)
    @Pattern(regexp = "[a-zA-Z]+",message = "must not contain special characters")
    private String prenom, adresse;

    @NotEmpty
    private String tel,date;

    private int age;

    @OneToMany(mappedBy = "client",fetch=FetchType.LAZY,cascade = CascadeType.REMOVE)
    private List<Rv> lrdvs;


    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", lrdvs=" + lrdvs +
                '}';
    }
}
