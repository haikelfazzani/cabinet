package com.nader.cabinet.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name="rv")
public class Rv {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String jour, description;
    @JoinColumn(name="ID_Client",referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Client client;
}
