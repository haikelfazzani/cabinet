package com.nader.cabinet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CabinetApplication {
	public static void main(String[] args) {
		try {
			SpringApplication.run(CabinetApplication.class, args);
		}
		catch (Exception e) {

		}
	}
}
